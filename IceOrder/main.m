//
//  main.m
//  IceOrder
//
//  Created by Jeriko on 5/22/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
